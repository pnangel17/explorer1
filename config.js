// Config code

const player_rotate_speed = 0.04; // Bigger number = faster!

const player_move_speed = 4; // Bigger number = faster!

const projectile_speed = 0.1; // Bigger number = faster!

const fps = 60; // depricated

const points_on_hit = 100;

const player_sprite_scale = 2.5; // Bigger number = larger sprite!

const pickup_sprite_scale = 1.75; // Bigger number = larger sprite!

const pickup_time_delay = 5; // Delay (in seconds) between pickups appearing

const bulletRechargeTime = 1; // Smaller number = faster fire rate

const bullet_scale = 1.5; // Bigger number = larger sprite

const bullet_lifespan = 200; // Bigger number = longer range

const integrator_step = 1.0; // depricated

const frictionCoeff = 0.025; // depricated
 
const player1Name = "Player 1";
const player2Name = "Player 2";
